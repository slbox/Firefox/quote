const rand = (max) => {
	return Math.round(Math.random() * max)
}

const notify = quote => {
	browser.notifications.create({
		type: "basic",
		title: quote.author,
		message: quote.line
	})
}

const quote = notifyBack => {
	fetch('db.json')
		.then(res => {
			return res.json()
		})
		.then(jsonArr => {
			const json = jsonArr[rand(jsonArr.length)]

			const quote = {
				author: json.author,
				line: json.quote
			};

			notifyBack(quote)
		})
		.catch(rej => {
			console.log(rej)
		})
}


//This is not the good way for FF extension
window.onoffline = e => quote(notify); 